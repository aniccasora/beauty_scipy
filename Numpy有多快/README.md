
# Numpy 有多快 

* 使用 Ipython magic 中的 timeit 計算``` 於此同時我的電腦還開著VM ```
```python
import numpy as np

# 0 ~ 1,000,000 的連續數字 ndarray.
array = np.arange(1e6)

# 的 python list 版本
list_array = array.tolist()
```


```python
# python list 集體操作
print("使用 Python list")
%timeit -n100 y = [val * 5 for val in list_array]

# numpy * 5  集體操作
print("\n使用 Numpy 操作")
%timeit -n100 x = array * 5
```
### 結果
    使用 Python list
    68.1 ms ± 2.27 ms per loop (mean ± std. dev. of 7 runs, 100 loops each)

    使用 Numpy 操作
    3.47 ms ± 353 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)

####  速度差近 20 倍