import numpy as np

# 0 ~ 1,000,000 的連續數字 ndarray.
array = np.arange(1e6)

# 的 python list 版本
list_array = array.tolist()

# python list 集體操作
y = [val * 5 for val in list_array]

x = array * 5