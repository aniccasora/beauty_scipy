# README #

本章所使用的 [資料數據集](https://github.com/elegant-scipy/elegant-scipy/blob/master/data/counts.txt.bz2)


### counts.txt

從 Cancer Genome Atlas [TCGA](http://cancergenome.nih.gov) 皮膚癌樣本取得的資料。

### 在這邊會用 pandas 讀取資料

1. pandas 強調表格，以及時間序列資料的處理，並可以處理多數值型態的陣列
2. 學習如何使用 pandas 取用資料，利用 col name 來取用我要的資料。
3. 使用 pandas.Index.intersection 來過濾 index.

後續可以參考 "Python for Data Analysis"
